import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        parse("a_input.txt", "a_output.txt");
        /*
         * parse("b_input.txt", "b_output.txt"); parse("c_input.txt", "c_output.txt");
         * parse("d_input.txt", "d_output.txt"); parse("e_input.txt", "e_output.txt");
         */
    }

    public static void parse(String input, String output) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input/" + input));
            PrintWriter writer = new PrintWriter("output/" + output);

            PovezanSeznam horizontalne = new PovezanSeznam();
            PovezanSeznam vertikalne = new PovezanSeznam();
            // parse data
            int numberOfPictures = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numberOfPictures; i++) {
                String line = reader.readLine();
                String[] data = line.split(" ");
                Picture newPicture = new Picture(i);
                newPicture.setHorizontal(data[0]);
                newPicture.makeSet(data);

                if (newPicture.horizontal) {
                    horizontalne.add(newPicture);
                } else {
                    vertikalne.add(newPicture);
                }
            }

            resi(horizontalne, vertikalne);

            reader.close();
            writer.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not found!");
        } catch (IOException ex) {
            System.out.println("IO exception!");
        }
    }

    public static void resi(PovezanSeznam horizontalne, PovezanSeznam vertikalne) {
        Slide prviSlide;
        if (horizontalne.first() == null) {
            prviSlide = new Slide(vertikalne.first().data);
            // dodamo dve vertikalni v slajd
            System.out.println("WTF");
        } else {
            // dodamo horizontalno v slajd
            prviSlide = new Slide(horizontalne.first().data);
        }
        Slide trenutniSlide = prviSlide;
        int bestScore, score;
        Node bestHorizontal, bestVertical1, bestVertical2;
        Node trenutnaSlika, bestNode;
        Set<String> presek;
        while (horizontalne.first() != null) {
            bestScore = 0;
            bestHorizontal = horizontalne.first();
            // primerjamo z horizontalnimi
            trenutnaSlika = horizontalne.first().next;
            while (trenutnaSlika != null) {
                if (trenutnaSlika.data.tags.size() / 2 < bestScore) {
                    break;
                }

                presek = dobiPresek(trenutniSlide.tags, trenutnaSlika.data.tags);
                score = minHorizontal(presek, trenutniSlide.tags, trenutnaSlika.data.tags);
                if (score >= bestScore) {
                    bestHorizontal = trenutnaSlika;
                    bestScore = score;
                }
                trenutnaSlika = trenutnaSlika.next;
            }
            System.out.println(bestHorizontal.data.id);
            trenutniSlide = new Slide(bestHorizontal.data);
            bestHorizontal.remove();
        }
    }

    public static int minHorizontal(Set<String> presek, Set<String> prvi, Set<String> drugi) {
        int velikostPreseka = presek.size();
        return Math.min(Math.min(velikostPreseka, prvi.size() - velikostPreseka), drugi.size() - velikostPreseka);
    }

    public static Set<String> dobiPresek(Set<String> prvi, Set<String> drugi) {
        Set<String> presek = new HashSet<String>(prvi);
        presek.retainAll(drugi);
        return presek;
    }
}

class Picture {
    boolean horizontal;
    Set<String> tags;
    int id;

    Picture(int id) {
        this.id = id;
    }

    public void setHorizontal(String argument) {
        if (argument.equals("H"))
            this.horizontal = true;
        else
            this.horizontal = false;
    }

    public void makeSet(
            String[] arguments) {/*
                                  * Set<String> s1 = new HashSet<String>(); s1.add("string1"); s1.add("string2");
                                  */
        Set<String> newTags = new HashSet<String>();
        for (int i = 2; i < arguments.length; i++) {
            newTags.add(arguments[i]);
        }
        this.tags = newTags;
    }
}

class Slide {
    boolean horizontal;
    Picture[] slike;
    Slide next;
    Set<String> tags;

    Slide(Picture slika) {
        this.horizontal = true;
        this.slike = new Picture[] { slika };
        this.tags = slika.tags;
    }

    Slide(Picture slika1, Picture slika2, Set<String> tags) {
        this.horizontal = false;
        this.slike = new Picture[] { slika1, slika2 };
        this.tags = tags;
    }
}

class PovezanSeznam {
    Node prazen; // first of list

    PovezanSeznam() {
        prazen = new Node(null);
    }

    // Adding a node at the front of the list
    public void add(Picture new_data) {
        Node newNode = new Node(new_data);

        Node start = prazen;
        while (start.next != null) {
            if (start.next.data.tags.size() < newNode.data.tags.size()) {
                newNode.prev = start;
                newNode.next = start.next;
                start.next.prev = newNode;
                start.next = newNode;

                return;
            }

            start = start.next;
        }

        start.next = newNode;
        newNode.prev = start;
    }

    public Node first() {
        return this.prazen.next;
    }

}

/* Doubly Linked list Node */
class Node {
    Picture data;
    Node prev;
    Node next;

    // Constructor to create a new node
    // next and prev is by default initialized as null
    Node(Picture d) {
        data = d;
    }

    Node() {
        data = null;
    }

    public void remove() {
        Node temp = new Node();
        temp = next;
        this.prev.next = this.next;
        if (temp != null)
            temp.prev = prev;
    }
}
